En la carpeta android se encuentra la aplicación.

En la carpeta apislim se encuentra la REST api.

En la carpeta db se encuentran dos ficheros de las tablas de la base de datos.



La parte web de la API está echa con APISLIM y se encuentra en https://yeray.alumno.club/

Utiliza una contraseña, por lo que solo se puede acceder a la api desde la aplicación.

Tiene 6 métodos:

- GET /alumns
    Devuelve una lista con todos los alumnos y sus comportamientos en el día en el que se encuentre. Si no hay ningún comportamiento, lo devuelve también pero con valores nulos. Para seleccionarlos hay un where deleted = 0,  ya que no se borran sino que se cambia el booleano

- POST /alumn
    Inserta un alumno


- PUT /alumn/id
    Modifica un alumno

- DELETE /alumn/id
    Borra el alumno (no lo borra como tal, cambia un booleano)

- POST /email
    Envía un correo electrónico

- POST /comportamiento
    Modifica o inserta el comportamiento con el día actual y para ello utiliza una sentencia INSERT ... ON DUPLICATE UPDATE ...


Para la parte móvil, he utilizado 5 activities:

- Main
    Tiene dos botones para navegar entre las otras actividades

    - Alumnos

        - Add/modify
            Añade o modifica un alumno

        - Send email
            Envía un correo al alumno, pudiendo elegir el asunto y el mensaje

    - Comportamiento
        Es un listview que muestra todos los alumnos, y tiene varios campos grisáceos (textviews) que al pulsarlos muestra un diálogo para modificarlo.