CREATE TABLE IF NOT EXISTS `keys` (
  `apikey` varchar(32) NOT NULL,
  PRIMARY KEY (`apikey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `keys` (`apikey`) VALUES
('12345'),
('abcde');
