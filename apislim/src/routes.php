<?php

// Routes

/*
$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
*/

$app->get("/", function ($request, $response) {

	$this->response->withHeader('Content-type', 'text/html; charset=UTF-8');
	return $this->response->write(
		"<h1>RESTful API Slim</h1>
		<a href ='doc/index.html'>API Documentation</a>");
});

// get alumnos
$app->get('/alumns', function ($request, $response) {

	$result = new Result();
	$key = $request->getHeader('apikey');
	if ($key[0] != "noselodigasanadie") {
		$result->setCode(FALSE);
		$result->setStatus(403);
		$result->setMessage("Error: " . "Forbidden access");
	}
	else {
		try {
			$dbquery =  $this->db->prepare("SELECT a.id, a.nombre, a.apellidos, a.direccion, a.ciudad, a.cp, a.telefono, a.mail, IFNULL(c.falta,'') as falta, IFNULL(c.trabajo,'') as trabajo, IFNULL(c.actitud,'') as actitud, IFNULL(c.observaciones,'') as observaciones FROM alumno a LEFT JOIN comportamiento c ON a.id = c.alumno WHERE a.deleted = 0 AND (c.fecha = CURRENT_DATE OR c.fecha IS NULL)");
			$dbquery->execute();
			$sites = $dbquery->fetchAll();
			$result->setCode(TRUE);
			$result->setStatus(OK);
			$result->setSites($sites);
		} catch (PDOException $e) {
			$result->setCode(FALSE);
			$result->setStatus(CONFLICT);
			$result->setMessage("Error: " . $e->getMessage());
		}
	}
	return $this->response->withJson($result);
});

// Put alumno
$app->post('/alumn', function ($request, $response) {

	$result = new Result();
	$key = $request->getHeader('apikey');
	if ($key[0] != "noselodigasanadie") {
		$result->setCode(FALSE);
		$result->setStatus(403);
		$result->setMessage("Error: " . "Forbidden access");
	}
	else {
		try {
			$input = $request->getParsedBody();
			$dbquery =  $this->db->prepare("INSERT INTO " . TABLE_ALUMNO . " (nombre, apellidos, direccion, ciudad, cp, telefono, mail) VALUES (?, ?, ?, ?, ?, ?, ?)");
			$dbquery->bindParam(1, $input['nombre']);
			$dbquery->bindParam(2, $input['apellidos']);
			$dbquery->bindParam(3, $input['direccion']);
			$dbquery->bindParam(4, $input['ciudad']);
			$dbquery->bindParam(5, $input['cp']);
			$dbquery->bindParam(6, $input['telefono']);
			$dbquery->bindParam(7, $input['mail']);
			$dbquery->execute();
			$number = $dbquery->rowCount();
			$lastId = $this->db->lastInsertId();
			if ($number > 0) {
				$result->setCode(TRUE);
				$result->setStatus(OK);
				$result->setLast($lastId);
			}
			else {
				$result->setCode(FALSE);
				$result->setStatus(NOT_COMPLETED);
				$result->setMessage("NOT INSERTED");
			}
		} catch (PDOException $e) {
			$result->setCode(FALSE);
			$result->setStatus(CONFLICT);
			$result->setMessage("Error: " . $e->getMessage());
		}
	}
	return $this->response->withJson($result);
});
// Put alumno (modify)
$app->put('/alumn/[{id}]', function ($request, $response, $args) {

	$result = new Result();
	$key = $request->getHeader('apikey');
	if ($key[0] != "noselodigasanadie") {
		$result->setCode(FALSE);
		$result->setStatus(403);
		$result->setMessage("Error: " . "Forbidden access");
	}
	else {
		try {
			$input = $request->getParsedBody();
			$dbquery = $this->db->prepare("UPDATE " . TABLE_ALUMNO . " SET nombre = ?, apellidos = ?, direccion = ?, ciudad = ?, cp = ?, telefono = ?, mail = ? WHERE id = ?");
			$dbquery->bindParam(1, $input['nombre']);
			$dbquery->bindParam(2, $input['apellidos']);
			$dbquery->bindParam(3, $input['direccion']);
			$dbquery->bindParam(4, $input['ciudad']);
			$dbquery->bindParam(5, $input['cp']);
			$dbquery->bindParam(6, $input['telefono']);
			$dbquery->bindParam(7, $input['mail']);
			$dbquery->bindParam(8, $args['id']);
			$dbquery->execute();
			$number = $dbquery->rowCount();

			if ($number > 0) {
				$result->setCode(TRUE);
				$result->setStatus(OK);
			}
			else {
				$result->setCode(FALSE);
				$result->setStatus(NOT_COMPLETED);
				$result->setMessage("NOT UPDATED");
			}
		} catch (PDOException $e) {
			$result->setCode(FALSE);
			$result->setStatus(CONFLICT);
			$result->setMessage("Error: " . $e->getMessage());
		}
	}
	return $this->response->withJson($result);
});

//Delete alumno
$app->delete('/alumn/[{id}]', function ($request, $response, $args) {
	$result = new Result();
	$key = $request->getHeader('apikey');
	if ($key[0] != "noselodigasanadie") {
		$result->setCode(FALSE);
		$result->setStatus(403);
		$result->setMessage("Error: " . "Forbidden access");
	}
	else {
		try {
			$dbquery = $this->db->prepare("UPDATE " . TABLE_ALUMNO . " SET deleted = 1 WHERE id = ?");
			$dbquery->bindParam(1,  $args['id']);
			$dbquery->execute();
			$number = $dbquery->rowCount();
			if ($number > 0) {
				$result->setCode(TRUE);
				$result->setStatus(OK);
			}
			else {
				$result->setCode(FALSE);
				$result->setStatus(NOT_COMPLETED);
				$result->setMessage("NOT DELETED");
			}
		} catch (PDOException $e) {
			$result->setCode(FALSE);
			$result->setStatus(CONFLICT);
			$result->setMessage("Error: " . $e->getMessage());
		}
	}
	return $this->response->withJson($result);
});

$app->post("/email", function ($request, $response) {

	$input = $request->getParsedBody();
	$result = sendEmail($input['from'], $input['password'], $input['to'], $input['subject'], $input['message']);
	return $this->response->withJson($result);
});

$app->post('/comportamiento', function ($request, $response) {

	$result = new Result();
	$key = $request->getHeader('apikey');
	if ($key[0] != "noselodigasanadie") {
		$result->setCode(FALSE);
		$result->setStatus(403);
		$result->setMessage("Error: " . "Forbidden access");
	}
	else {
		try {
			$this->db->beginTransaction();
			$input = $request->getParsedBody();
			$dbquery =  $this->db->prepare($input['sql']);
			$dbquery->execute();
			$lastId = $this->db->lastInsertId();
			$result->setCode(TRUE);
			$result->setStatus(OK);
			$result->setLast($lastId);
			$this->db->commit();
		} catch (PDOException $e) {
			$result->setCode(FALSE);
			$result->setStatus(CONFLICT);
			$result->setMessage("Error: " . $e->getMessage());
    		$this->db->rollBack();
		}
	}
	return $this->response->withJson($result);
});

function sendEmail($from, $password, $to, $subject, $message) {

	$result = new Result();
	$mail = new PHPMailer(true); 	// the true param means it will throw exceptions on errors, which we need to catch
	$mail->IsSMTP(); 		// telling the class to use SMTP

	try {
	    //$mail->SMTPDebug  = 2;			// enables SMTP debug information (for testing)
	    $mail->SMTPDebug  = 0;
	    $mail->SMTPAuth   = true;			// enable SMTP authentication
	    $mail->SMTPSecure = "None";                 	// sets the prefix to the server
	    $mail->Host       ="mail.portadaalta.info";
	    //$mail->Host       = "smtp.gmail.com";      	// sets GMAIL as the SMTP server
	    //$mail->Host       = "smtp.openmailbox.org";
	    $mail->Port       = 25;                   	// set the SMTP port for the GMAIL server
	    $mail->Username   = $from;			// GMAIL username
	    $mail->Password   = $password;		// GMAIL password
	    $mail->AddAddress($to);			// Receiver email
	    $mail->SetFrom($from, 'paco g.');		// email sender
	    $mail->AddReplyTo($from, 'paco g.');		// email to reply
	    $mail->Subject = $subject;			// subject of the message
	    $mail->AltBody = 'Message in plain text';	// optional - MsgHTML will create an alternate automatically
	    $mail->MsgHTML($message);			// message in the email
	    //$mail->AddAttachment('images/phpmailer.gif');      // attachment
	    $mail->Send();
	    $result->setCode(TRUE);
	    $result->setMessage("Message Sent OK to " . $to);
	} catch (phpmailerException $e) {
	    //echo $e->errorMessage(); 			//Pretty error messages from PHPMailer
	    $result->setCode(FALSE);
    	$result->setMessage("Error: " . $e->errorMessage());
	} catch (Exception $e) {
	    //echo $e->getMessage(); 			//Boring error messages from anything else!
	    $result->setCode(FALSE);
    	$result->setMessage("Error: " . $e->getMessage());
	}
	return $result;
}

