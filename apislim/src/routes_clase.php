<?php
// Routes

/*
$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
*/
$app->get("/", function ($request, $response) {
return $this->response->write(
"<h1>RESTful API Slim</h1>
<a href ='doc/index.html'>API Documentation</a>");
});

// get all sites
$app->get('/site', function ($request, $response) {
$result = new Result();
try {
$dbquery = $this->db->prepare("SELECT * FROM " . TABLE);
$dbquery->execute();
$sites = $dbquery->fetchAll();
$result->setCode(TRUE);
$result->setStatus(OK);
$result->setSites($sites);
} catch (PDOException $e) {
$result->setCode(FALSE);
$result->setStatus(CONFLICT);
$result->setMessage("Error: " . $e->getMessage());
}
return $this->response->withJson($result);
});

$app->get('/site/[{id}]', function ($request, $response, $args) {
$result = new Result();
try {
$dbquery = $this->db->prepare("SELECT * FROM " . TABLE . " WHERE id = ?");
$dbquery->bindParam(1, $args['id']);
$dbquery->execute();
$site = $dbquery->fetchObject();
if ($site != null) {
$result->setCode(TRUE);
$result->setStatus(OK);
$result->setSites($site);
}
else {
$result->setCode(FALSE);
$result->setStatus(NOT_COMPLETED);
$result->setMessage("Does the site exist?");
}
} catch (PDOException $e) {
$result->setCode(FALSE);
$result->setStatus(CONFLICT);
$result->setMessage("Error: " . $e->getMessage());
}
return $this->response->withJson($result);
});

$app->post('/site', function ($request, $response) {
$result = new Result();
//lock the table
try {
$input = $request->getParsedBody();
$dbquery = $this->db->prepare("INSERT INTO " . TABLE . " (name, link, email) VALUES (?, ?, ?)");
$dbquery->bindParam(1, $input['name']);
$dbquery->bindParam(2, $input['link']);
$dbquery->bindParam(3, $input['email']);
$dbquery->execute();
$number = $dbquery->rowCount();
$lastId = $this->db->lastInsertId();
if ($number > 0) {
$result->setCode(TRUE);
$result->setStatus(OK);
$result->setLast($lastId);
}
else {
$result->setCode(FALSE);
$result->setStatus(NOT_COMPLETED);
$result->setMessage("NOT INSERTED");
}
} catch (PDOException $e) {
$result->setCode(FALSE);
$result->setStatus(CONFLICT);
$result->setMessage("Error: " . $e->getMessage());
}
return $this->response->withJson($result);
});

$app->put('/site/[{id}]', function ($request, $response, $args) {
$result = new Result();
try {
$input = $request->getParsedBody();
$dbquery = $this->db->prepare("UPDATE " . TABLE . " SET name = ?, link = ?, email = ? WHERE id = ?");
$dbquery->bindParam(1, $input['name']);
$dbquery->bindParam(2, $input['link']);
$dbquery->bindParam(3, $input['email']);
$dbquery->bindParam(4, $args['id']);
$dbquery->execute();
$number = $dbquery->rowCount();
if ($number > 0) {
$result->setCode(TRUE);
$result->setStatus(OK);
}
else {
$result->setCode(FALSE);
$result->setStatus(NOT_COMPLETED);
$result->setMessage("NOT UPDATED");
}
} catch (PDOException $e) {
$result->setCode(FALSE);
$result->setStatus(CONFLICT);
$result->setMessage("Error: " . $e->getMessage());
}
return $this->response->withJson($result);
});

$app->delete('/site/[{id}]', function ($request, $response, $args) {
$result = new Result();
try {
$dbquery = $this->db->prepare("DELETE FROM " . TABLE . " WHERE id = ?");
$dbquery->bindParam(1, $args['id']);
$dbquery->execute();
$number = $dbquery->rowCount();
if ($number > 0) {
$result->setCode(TRUE);
$result->setStatus(OK);
}
else {
$result->setCode(FALSE);
$result->setStatus(NOT_COMPLETED);
$result->setMessage("NOT DELETED");
}
} catch (PDOException $e) {
$result->setCode(FALSE);
$result->setStatus(CONFLICT);
$result->setMessage("Error: " . $e->getMessage());
}
return $this->response->withJson($result);
});
