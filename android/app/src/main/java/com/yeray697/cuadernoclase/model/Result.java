package com.yeray697.cuadernoclase.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yeray697 on 28/02/17.
 */

public class Result implements Serializable {
    private boolean code;
    private int status;
    private String message;
    private ArrayList<Alumno> alumnos;
    private int last;

    public boolean getCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(ArrayList<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }
}