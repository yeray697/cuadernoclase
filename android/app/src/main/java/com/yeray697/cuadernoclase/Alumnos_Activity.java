package com.yeray697.cuadernoclase;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.yeray697.cuadernoclase.adapter.Alumno_Adapter;
import com.yeray697.cuadernoclase.model.Alumno;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Alumnos_Activity extends AppCompatActivity {

    public static final String ALUMNO_MODIFIED_POSITION = "ALUMNO_MODIFIED_POSITION";
    public static final String ALUMNO_MODIFIED = "ALUMNO_MODIFIED";
    public static final String ALUMNO_TO_ADD = "ALUMNO_TO_ADD";
    public static final String MAIL = "mail";

    private static final int MODIFY_CODE = 1;
    private static final int ADD_CODE = 2;

    private ListView lvAlumnos;
    private FloatingActionButton fabAdd;

    private static Alumno_Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumnos);
        setTitle("Alumnos");
        fabAdd = (FloatingActionButton) findViewById(R.id.fabAddAlumno);
        lvAlumnos = (ListView) findViewById(R.id.lvAlumnos);
        adapter = new Alumno_Adapter(this);
        lvAlumnos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sendMail(adapter.getItem(position).getMail());
            }
        });
        lvAlumnos.setAdapter(adapter);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAlumno();
            }
        });
        registerForContextMenu(lvAlumnos);
    }

    private void sendMail(String mail) {
        Intent intent = new Intent(Alumnos_Activity.this,SendMail_Activity.class);
        intent.putExtra(MAIL,mail);
        startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.alumno_contextual_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int listPosition = info.position;
        switch (item.getItemId()) {
            case R.id.action_eliminar_alumno:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("¿Seguro que deseas borrarlo?");
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeAlumno(listPosition);
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
                return true;
            case R.id.action_modificar_alumno:
                modifyAlumno(listPosition);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            Bundle bundle = data.getExtras();
            if (requestCode == MODIFY_CODE){
                int position = bundle.getInt(ALUMNO_MODIFIED_POSITION);
                Alumno alumnoModified = (Alumno) bundle.getSerializable(ALUMNO_MODIFIED);
                adapter.modifyAlumno(alumnoModified);
            } else if (requestCode == ADD_CODE){
                Alumno alumno = (Alumno) bundle.getSerializable(ALUMNO_TO_ADD);
                adapter.addAlumno(alumno);
            }
        }
    }

    private void removeAlumno(int position) {
        adapter.removeAlumno(adapter.getItem(position));
    }

    private void modifyAlumno(int position) {
        Alumno alumno = adapter.getItem(position);
        Intent intent = new Intent(Alumnos_Activity.this, ManageAlumno_Activity.class);
        intent.putExtra(ALUMNO_MODIFIED_POSITION,position);
        intent.putExtra(ALUMNO_MODIFIED,alumno);
        startActivityForResult(intent,MODIFY_CODE);
    }

    private void addAlumno() {
        Intent intent = new Intent(Alumnos_Activity.this, ManageAlumno_Activity.class);
        startActivityForResult(intent,ADD_CODE);
    }

    public static Alumno_Adapter getAdapter() {
        return adapter;
    }
}
