package com.yeray697.cuadernoclase;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yeray697.cuadernoclase.model.Alumno;

import java.util.Random;

public class ManageAlumno_Activity extends AppCompatActivity {

    private int position;
    private Alumno alumno;
    private EditText etNombre, etApellidos, etDireccion, etCiudad, etCp, etTelefono, etMail;
    private Button btSaveAlumno;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_alumno);
        etNombre = (EditText) findViewById(R.id.etNombre);
        etApellidos = (EditText) findViewById(R.id.etApellidos);
        etDireccion = (EditText) findViewById(R.id.etDireccion);
        etCiudad = (EditText) findViewById(R.id.etCiudad);
        etCp = (EditText) findViewById(R.id.etCp);
        etTelefono = (EditText) findViewById(R.id.etTelefono);
        etMail = (EditText) findViewById(R.id.etMail);
        btSaveAlumno = (Button) findViewById(R.id.btSaveAlumno);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            position = extras.getInt(Alumnos_Activity.ALUMNO_MODIFIED_POSITION,-1);
            if (position != -1) {
                alumno = (Alumno) extras.getSerializable(Alumnos_Activity.ALUMNO_MODIFIED);
            }
        } else {
            position = -1;
        }
        if (alumno != null){
            setTitle("Modificar");
            btSaveAlumno.setText("Modificar");
            etNombre.setText(alumno.getNombre());
            etApellidos.setText(alumno.getApellidos());
            etDireccion.setText(alumno.getDireccion());
            etCiudad.setText(alumno.getCiudad());
            etCp.setText(alumno.getCp());
            etTelefono.setText(alumno.getTelefono());
            etMail.setText(alumno.getMail());
        } else {
            setTitle("Añadir");
            btSaveAlumno.setText("Añadir");
        }
        btSaveAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAlumno();
            }
        });
    }

    private void saveAlumno() {
            try {
                int id;
                if (position == -1) {
                    id  = Math.abs(new Random().nextInt());
                } else {
                    id = Integer.parseInt(alumno.getId());
                }
                alumno = new Alumno(String.valueOf(id),etNombre.getText().toString(),
                        etApellidos.getText().toString(),etCiudad.getText().toString(),etCp.getText().toString(),
                        etDireccion.getText().toString(),etMail.getText().toString(),etTelefono.getText().toString());
                finishActivity();
            } catch (Exception e) {
                Toast.makeText(this, "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
    }

    private void finishActivity(){
        Intent returnIntent = new Intent();
        if (position == -1){
            returnIntent.putExtra(Alumnos_Activity.ALUMNO_TO_ADD,alumno);

        } else{
            returnIntent.putExtra(Alumnos_Activity.ALUMNO_MODIFIED_POSITION,position);
            returnIntent.putExtra(Alumnos_Activity.ALUMNO_MODIFIED,alumno);

        }
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}
