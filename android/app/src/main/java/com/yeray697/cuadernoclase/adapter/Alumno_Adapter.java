package com.yeray697.cuadernoclase.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yeray697.cuadernoclase.Alumno_Application;
import com.yeray697.cuadernoclase.ApiMethods;
import com.yeray697.cuadernoclase.R;
import com.yeray697.cuadernoclase.model.Alumno;

import java.util.ArrayList;

/**
 * Created by usuario on 9/02/17.
 */

public class Alumno_Adapter extends ArrayAdapter<Alumno> {
    public Alumno_Adapter(Context context) {
        super(context, R.layout.alumno_item, ((Alumno_Application)context.getApplicationContext()).getAlumnos());
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        AlumnoHolder holder;
        if (view == null){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alumno_item,parent,false);
            holder = new AlumnoHolder();
            holder.tvAlumno = (TextView) view.findViewById(R.id.tvAlumno_item);
            view.setTag(holder);
        } else {
            holder = (AlumnoHolder) view.getTag();
        }
        holder.tvAlumno.setText(getItem(position).toString());
        return view;
    }

    public void removeAlumno(Alumno alumno) {
        ApiMethods.removeAlumno(getContext(), alumno);
    }

    public void modifyAlumno( Alumno newAlumno) {
        ApiMethods.modifyAlumno(getContext(), newAlumno);
        notifyDataSetChanged();
    }

    public  void addAlumno(Alumno alumno){
        ApiMethods.addAlumno(getContext(), alumno);
    }
    public ArrayList<Alumno> getList() {
        ArrayList<Alumno> list = new ArrayList<>();
        for (int i = 0; i < getCount(); i++)
            list.add(getItem(i));
        return list;
    }

    public void updateList() {
        clear();
        addAll(((Alumno_Application)getContext().getApplicationContext()).getAlumnos());
        notifyDataSetChanged();
    }

    private class AlumnoHolder{
        TextView tvAlumno;
    }
}
