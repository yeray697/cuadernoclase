package com.yeray697.cuadernoclase.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Patterns;

import java.io.Serializable;

/**
 * Created by usuario on 9/02/17.
 */
public class Alumno implements Serializable{
    private String id;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String ciudad;
    private String cp;
    private String telefono;
    private String mail;
    private String falta;
    private String trabajo;
    private String actitud;
    private String observaciones;

    public Alumno(){

    }

    public Alumno(String id, String nombre, String apellidos, String direccion, String ciudad, String cp, String telefono, String mail, String falta, String trabajo, String actitud, String observaciones) throws Exception {
        this.setId(id);
        this.setNombre(nombre);
        this.setApellidos(apellidos);
        this.setDireccion(direccion);
        this.setCiudad(ciudad);
        this.setCp(cp);
        this.setTelefono(telefono);
        this.setMail(mail);
        this.setFalta(falta);
        this.setTrabajo(trabajo);
        this.setActitud(actitud);
        this.setObservaciones(observaciones);
    }

    public Alumno(String id, String nombre, String apellidos, String ciudad, String cp,
                  String direccion, String mail, String telefono) throws Exception {
        this.setId(id);
        this.setNombre(nombre);
        this.setApellidos(apellidos);
        this.setDireccion(direccion);
        this.setCiudad(ciudad);
        this.setCp(cp);
        this.setTelefono(telefono);
        this.setMail(mail);
        this.setFalta("Sin falta");
        this.setTrabajo(" ");
        this.setActitud(" ");
        this.setObservaciones("");
    }

    public String getApellidos() {
        return this.apellidos;
    }

    public String getCiudad() {
        return this.ciudad;
    }

    public String getCp() {
        return this.cp;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public String getId() {
        return this.id;
    }

    public String getMail() {
        return this.mail;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public String getFalta() {
        return falta;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public String getActitud() {
        return actitud;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setApellidos(String apellidos) throws Exception {
        if (TextUtils.isEmpty(apellidos))
            throw new Exception("El campo \"Apellidos\" no puede estar vacío");
        this.apellidos = apellidos;
    }

    public void setCiudad(String ciudad) throws Exception {
        if (TextUtils.isEmpty(ciudad))
            throw new Exception("El campo \"Ciudad\" no puede estar vacío");
        this.ciudad = ciudad;
    }

    public void setCp(String cp) throws Exception {
        if (TextUtils.isEmpty(cp))
            throw new Exception("El campo \"Código postal\" no puede estar vacío");
        this.cp = cp;
    }

    public void setDireccion(String direccion) throws Exception {
        if (TextUtils.isEmpty(direccion))
            throw new Exception("El campo \"Dirección\" no puede estar vacío");
        this.direccion = direccion;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMail(String mail) throws Exception {
        if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches())
            throw new Exception("El correo electrónico es inválido");
        this.mail = mail;
    }

    public void setNombre(String nombre) throws Exception {
        if (TextUtils.isEmpty(nombre))
            throw new Exception("El campo \"Nombre\" no puede estar vacío");
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) throws Exception {
        if (TextUtils.isEmpty(telefono))
            throw new Exception("El campo \"Teléfono\" no puede estar vacío");
        this.telefono = telefono;
    }

    public void setFalta(String falta) throws Exception {
        if (falta == null)
            this.falta = "";
        else
            this.falta = falta;
    }

    public void setTrabajo(String trabajo) throws Exception {
        if (trabajo == null)
            this.trabajo = "";
        else
            this.trabajo = trabajo;
    }

    public void setActitud(String actitud) throws Exception {
        if (actitud == null)
            this.actitud = "";
        else
            this.actitud = actitud;
    }

    public void setObservaciones(String observaciones) throws Exception {
        if (observaciones == null)
            this.observaciones = "";
        else
            this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return apellidos + ", " + nombre;
    }

    public static Alumno cloneAlumno(Alumno alumno) {
        Alumno cloned = null;
        try {
            cloned = new Alumno(
                    alumno.getId(),alumno.getNombre(),alumno.getApellidos(),alumno.getCiudad(),alumno.getCp(),alumno.getDireccion(),alumno.getMail(),alumno.getTelefono());
            cloned.setFalta(alumno.getFalta());
            cloned.setTrabajo(alumno.getTrabajo());
            cloned.setActitud(alumno.getActitud());
            cloned.setObservaciones(alumno.getObservaciones());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cloned;
    }

    @Override
    public boolean equals(Object v) {
        boolean equals = false;

        if (v instanceof Alumno){
            Alumno ptr = (Alumno) v;
            equals = ptr.id.equals(this.id);
            if (equals){
                if (equals) {
                    equals = ptr.falta.equalsIgnoreCase(this.falta);
                    if (equals) {
                        equals = ptr.trabajo.equalsIgnoreCase(this.trabajo);
                        if (equals) {
                            equals = ptr.actitud.equalsIgnoreCase(this.actitud);
                            if (equals) {
                                equals = ptr.observaciones.equalsIgnoreCase(this.observaciones);
                                if (equals) {
                                    equals = ptr.apellidos.equalsIgnoreCase(this.apellidos);
                                    if (equals) {
                                        equals = ptr.nombre.equalsIgnoreCase(this.nombre);
                                        if (equals) {
                                            equals = ptr.direccion.equalsIgnoreCase(this.direccion);
                                            if (equals) {
                                                equals = ptr.ciudad.equalsIgnoreCase(this.ciudad);
                                                if (equals) {
                                                    equals = ptr.cp.equalsIgnoreCase(this.cp);
                                                    if (equals) {
                                                        equals = ptr.telefono.equalsIgnoreCase(this.telefono);
                                                        if (equals) {
                                                            equals = ptr.mail.equalsIgnoreCase(this.mail);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                /*
                    mail;
                    falta;
                    trabajo;
                    actitud;
                    observaciones;
                 */
            }
        }

        return equals;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Integer.parseInt(this.id);
        return hash;
    }
}
