package com.yeray697.cuadernoclase;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.yeray697.cuadernoclase.adapter.Faltas_Adapter;
import com.yeray697.cuadernoclase.model.Alumno;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Table_Activity extends AppCompatActivity {

    private ListView lvFaltas;
    private static Faltas_Adapter adapter;
    private TextView tvFecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        setTitle("Comportamiento");
        tvFecha =(TextView) findViewById(R.id.tvFecha);
        lvFaltas = (ListView) findViewById(R.id.lvFaltas);
        adapter = new Faltas_Adapter(this);
        lvFaltas.setAdapter(adapter);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MM-yyyy");
        String fecha = sdf.format(Calendar.getInstance().getTime());
        fecha = fecha.substring(0, 1).toUpperCase() + fecha.substring(1);
        tvFecha.setText(fecha);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.faltas_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_faltas:
                modifyFaltasAlumno(getAlumnosModificados());
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void modifyFaltasAlumno(ArrayList<Alumno> alumnosModificados) {
        ApiMethods.modifyFaltasAlumno(this, alumnosModificados);
    }

    public ArrayList<Alumno> getAlumnosModificados() {
        ArrayList<Alumno> newList = adapter.getList();
        ArrayList<Alumno> oldList = ((Alumno_Application) getApplicationContext()).getAlumnos();
        ArrayList<Alumno> alumnosModificados = new ArrayList<>();
        if(newList==null || oldList.size()!=newList.size())
            return newList;

        for(Alumno itemList1: newList)
        {
            if(!oldList.contains(itemList1))
                alumnosModificados.add(itemList1);
        }

        return alumnosModificados;
    }

    @Override
    public void onBackPressed() {
        if (getAlumnosModificados().size() > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Hay cambios sin guardar");
            builder.setMessage("¿Deseas salir? Perderás el trabajo realizado");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        } else
            super.onBackPressed();
    }

    public static Faltas_Adapter getAdapter() {
        return adapter;
    }
}
