package com.yeray697.cuadernoclase;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SendMail_Activity extends AppCompatActivity {

    private String mail;
    private TextView tvMail;
    private EditText etAsunto;
    private EditText etMensaje;
    private Button btSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_mail);
        setTitle("Enviar correo");
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            mail = extras.getString(Alumnos_Activity.MAIL);
        if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()){
            Toast.makeText(this, "Error, el correo está vacío", Toast.LENGTH_SHORT).show();
            finish();
        }
        tvMail = (TextView) findViewById(R.id.tvMail_mail);
        etAsunto = (EditText) findViewById(R.id.etAsunto_mail);
        etMensaje = (EditText) findViewById(R.id.etMensaje_mail);
        btSend = (Button) findViewById(R.id.btEnviar_mail);

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String asunto = etAsunto.getText().toString();
                String mensaje = etMensaje.getText().toString();
                ApiMethods.sendMail(SendMail_Activity.this,mail,asunto,mensaje);
            }
        });
        tvMail.setText(mail);
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aún no lo has enviado");
        builder.setMessage("¿Deseas descartar el correo?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}
