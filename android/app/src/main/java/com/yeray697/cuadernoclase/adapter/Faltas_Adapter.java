package com.yeray697.cuadernoclase.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.yeray697.cuadernoclase.Alumno_Application;
import com.yeray697.cuadernoclase.R;
import com.yeray697.cuadernoclase.model.Alumno;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yeray697 on 28/02/17.
 */

public class Faltas_Adapter extends ArrayAdapter<Alumno> {

    public Faltas_Adapter(Context context) {
        super(context, R.layout.faltas_item, new ArrayList<>(((Alumno_Application)context.getApplicationContext()).getAlumnosClonados()));
    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        FaltasHolder holder;
        if (view == null){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.faltas_item,parent,false);
            holder = new FaltasHolder();
            holder.tvAlumno = (TextView) view.findViewById(R.id.tvAlumnoNombre_item);
            holder.tvFalta = (TextView) view.findViewById(R.id.tvFalta_item);
            holder.tvTrabajo = (TextView) view.findViewById(R.id.tvTrabajo_item);
            holder.tvActitud = (TextView) view.findViewById(R.id.tvActitud_item);
            holder.tvObservaciones = (TextView) view.findViewById(R.id.tvObservaciones_item);
            view.setTag(holder);
        } else {
            holder = (FaltasHolder) view.getTag();
        }
        final Alumno alumno = getItem(position);
        holder.tvFalta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRadioButtonDialog("Falta",alumno);
            }
        });
        holder.tvTrabajo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRadioButtonDialog("Trabajo",alumno);
            }
        });
        holder.tvActitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRadioButtonDialog("Actitud",alumno);
            }
        });

        holder.tvObservaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditTextDialog("Observaciones",alumno);
            }
        });
        holder.tvAlumno.setText(alumno.toString());
        holder.tvFalta.setText(String.valueOf(alumno.getFalta()));
        holder.tvTrabajo.setText(String.valueOf(alumno.getTrabajo()));
        holder.tvActitud.setText(String.valueOf(alumno.getActitud()));
        holder.tvObservaciones.setText(alumno.getObservaciones());
        return view;
    }
    public ArrayList<Alumno> getList() {
        ArrayList<Alumno> list = new ArrayList<>();
        for (int i = 0; i < getCount(); i++)
            list.add(getItem(i));
        return list;
    }

    private class FaltasHolder{
        TextView tvAlumno;
        TextView tvFalta;
        TextView tvTrabajo;
        TextView tvActitud;
        TextView tvObservaciones;
    }


    private void showEditTextDialog(String accion, final Alumno alumno) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.edittext_dialog, null);
        dialog.setView(dialogView);

        dialog.setTitle(accion);
        final EditText etObservaciones = (EditText) dialogView.findViewById(R.id.etObservaciones_dialog);
        etObservaciones.setText(alumno.getObservaciones());

        dialog.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    alumno.setObservaciones(etObservaciones.getText().toString());
                    notifyDataSetChanged();
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

    private void showRadioButtonDialog(final String accion, final Alumno alumno) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.radiobutton_dialog, null);
        dialog.setView(dialogView);
        dialog.setTitle(accion);
        final AlertDialog d = dialog.show();
        final List<String> stringList=new ArrayList<>();
        if (accion.equals("Falta")){
            stringList.add("Sin falta");
            stringList.add("Injustificada");
            stringList.add("Justificada");
            stringList.add("Retraso");
        } else if (accion.equals("Trabajo")){
            stringList.add("Bueno");
            stringList.add("Regular");
            stringList.add("Malo");
        } else if (accion.equals("Actitud")){
            stringList.add("Positiva");
            stringList.add("Negativa");
        }
        RadioGroup rg = (RadioGroup) dialogView.findViewById(R.id.radio_group);

        for(int i=0;i<stringList.size();i++){
            RadioButton rb=new RadioButton(getContext());
            rb.setText(stringList.get(i));
            rb.setId(i);
            boolean checked = false;
            if (accion.equals("Falta")){
                checked = (alumno.getFalta().equals("")) ? false : rb.getText().toString().startsWith(String.valueOf(alumno.getFalta()));
            } else if (accion.equals("Trabajo")){
                checked = !(alumno.getTrabajo().equals(" ") || alumno.getTrabajo().equals("")) && rb.getText().toString().startsWith(String.valueOf(alumno.getTrabajo()));
            } else if (accion.equals("Actitud")){
                checked = !(alumno.getActitud().equals(" ")|| alumno.getActitud().equals("")) && rb.getText().toString().startsWith(String.valueOf(alumno.getActitud()));
            }
            rb.setChecked(checked);
            rg.addView(rb);
        }
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (accion.equals("Falta")){
                    String falta = " ";
                    if (checkedId == 0){
                        falta = "Sin falta";
                    } else if (checkedId == 1){
                        falta = "Injustificada";
                    } else if (checkedId == 2){
                        falta = "Justificada";
                    } else if (checkedId == 3){
                        falta = "Retraso";
                    }
                    try {
                        alumno.setFalta(falta);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (accion.equals("Trabajo")){
                    String trabajo = " ";
                    if (checkedId == 0){
                        trabajo = "Bueno";
                    } else if (checkedId == 1){
                        trabajo = "Regular";
                    } else if (checkedId == 2){
                        trabajo = "Malo";
                    }
                    try {
                        alumno.setTrabajo(trabajo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (accion.equals("Actitud")){
                    String actitud = " ";
                    if (checkedId == 0){
                        actitud = "Positiva";
                    } else if (checkedId == 1){
                        actitud = "Negativa";
                    }
                    try {
                        alumno.setActitud(actitud);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                notifyDataSetChanged();
                d.dismiss();
            }
        });
    }
}
