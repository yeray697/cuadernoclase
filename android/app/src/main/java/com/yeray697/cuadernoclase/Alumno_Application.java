package com.yeray697.cuadernoclase;

import android.app.Application;

import com.yeray697.cuadernoclase.model.Alumno;

import java.util.ArrayList;

/**
 * Created by yeray697 on 28/02/17.
 */

public class Alumno_Application extends Application {
    private static ArrayList<Alumno> alumnos;

    @Override
    public void onCreate() {
        super.onCreate();
        alumnos = new ArrayList<>();
        try {
            ApiMethods.getAlumnos(this);
            //alumnos.add(new Alumno(1,"pepito","de los palotes","hsh","shsh","hsj","yeray.1997.yr@gmail.com","wyw"));
            //alumnos.add(new Alumno(2,"pepito","de los palotes","hsh","shsh","hsj","yeray.1997.yr@gmail.com","wyw"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Alumno> getAlumnos() {
        return alumnos;
    }
    public ArrayList<Alumno> getAlumnosClonados() {
        ArrayList<Alumno> clone = new ArrayList<Alumno>(alumnos.size());
            try {
                Alumno aux;
                for (Alumno item : alumnos) {
                    aux = Alumno.cloneAlumno(item);
                    if (aux != null)
                        clone.add(aux);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        return clone;
    }

    public static void addAlumno(Alumno alumno) {
        alumnos.add(alumno);
    }

    public static void removeAlumno(Alumno alumno) {
        alumnos.remove(alumno);
    }

    public static void setAlumnos(ArrayList<Alumno> newAlumnos){
        alumnos = newAlumnos;
    }
}
