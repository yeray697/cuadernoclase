package com.yeray697.cuadernoclase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btMostrarAlumnos;
    Button btMostrarIncidencias;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btMostrarAlumnos = (Button) findViewById(R.id.btMostrarAlumnos);
        btMostrarIncidencias = (Button) findViewById(R.id.btMostrarIncidencias);

        btMostrarAlumnos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Alumnos_Activity.class);
            }
        });

        btMostrarIncidencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(Table_Activity.class);
            }
        });
    }

    private void openActivity (Class<?> activity){
        Intent intent = new Intent(MainActivity.this, activity);
        startActivity(intent);
    }
}
