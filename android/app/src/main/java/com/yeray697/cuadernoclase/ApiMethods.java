package com.yeray697.cuadernoclase;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yeray697.cuadernoclase.adapter.Alumno_Adapter;
import com.yeray697.cuadernoclase.adapter.Faltas_Adapter;
import com.yeray697.cuadernoclase.model.Alumno;
import com.yeray697.cuadernoclase.model.Result;
import com.yeray697.cuadernoclase.utils.RestClient;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by yeray697 on 28/02/17.
 */

public class ApiMethods {
    private static final String FROM = "alumno@portadaalta.info";
    private static final String PASSWORD = "malaga2017";

    private static final String URL_GET = "alumns";
    public static final String URL_ADD = "alumn";
    public static final String URL_MAIL = "email";
    private static final String URL_REMOVE = "alumn";
    private static final String URL_MODIFY = "alumn";
    private static final String URL_MODIFY_FALTAS = "comportamiento";

    public static void addAlumno(final Context context, Alumno alumno) {
        final ProgressDialog progreso = new ProgressDialog(context);
        RequestParams params = new RequestParams();
        params.put("nombre", alumno.getNombre());
        params.put("apellidos", alumno.getApellidos());
        params.put("direccion", alumno.getDireccion());
        params.put("ciudad", alumno.getCiudad());
        params.put("cp", alumno.getCp());
        params.put("telefono", alumno.getTelefono());
        params.put("mail", alumno.getMail());
        RestClient.post(URL_ADD, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Añadiendo . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Añadido correctamente";
                        getAlumnos(context);
                    } else
                        message = "Error añadiendo el alumno:\n" + result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void removeAlumno(final Context context, Alumno alumno) {
        final ProgressDialog progreso = new ProgressDialog(context);
        RestClient.delete(URL_REMOVE+"/"+alumno.getId(),new JsonHttpResponseHandler(){
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Eliminando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Eliminado correctamente";
                        getAlumnos(context);
                    } else
                        message = "Error eliminando el alumno:\n" + result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void modifyAlumno(final Context context, Alumno newAlumno) {
        final ProgressDialog progreso = new ProgressDialog(context);
        RequestParams params = new RequestParams();
        params.put("nombre", newAlumno.getNombre());
        params.put("apellidos", newAlumno.getApellidos());
        params.put("direccion", newAlumno.getDireccion());
        params.put("ciudad", newAlumno.getCiudad());
        params.put("cp", newAlumno.getCp());
        params.put("telefono", newAlumno.getTelefono());
        params.put("mail", newAlumno.getMail());
        RestClient.put(URL_MODIFY+"/"+newAlumno.getId(),params,new JsonHttpResponseHandler(){
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Modificando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Modificado correctamente";
                        getAlumnos(context);
                    } else
                        message = "Error modificando el alumno:\n" + result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void modifyFaltasAlumno(final Context context, final ArrayList<Alumno> alumnos) {
        final ProgressDialog progreso = new ProgressDialog(context);
        String sql1 = "INSERT INTO `comportamiento`(`alumno`, `fecha`, `falta`, `trabajo`, `actitud`, `observaciones`) VALUES ";
        String sql2 = ""; //(?,?,?,?,?,?), (?,?,?,?,?,?), ...
        String sql3 = "ON DUPLICATE KEY UPDATE falta = VALUES(falta), trabajo = VALUES(trabajo), actitud = VALUES(actitud), observaciones = VALUES(observaciones)";
        Alumno alumno;
        for (int i = 0; i < alumnos.size(); i++) {
            alumno = alumnos.get(i);
            if (i != 0)
                sql2 += ",";
            sql2 += "("+alumno.getId()+",CURRENT_DATE,'"+alumno.getFalta()+"','"+alumno.getTrabajo()+"','"+alumno.getActitud()+"','"+alumno.getObservaciones()+"') ";
        }
        String sql = sql1 + sql2 + sql3;

        RequestParams params = new RequestParams();
        params.put("sql", sql);
        RestClient.post(URL_MODIFY_FALTAS, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Guardando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Guardado correctamente";
                        getAlumnos(context);
                    } else
                        message = "Error guardando el/los alumno(s):\n" + result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void getAlumnos(final Context context){
        RestClient.get(URL_GET, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Result result;
                Gson gson = new Gson();
                String responseString = String.valueOf(response);
                result = gson.fromJson(responseString, Result.class);
                if (result != null)
                    if (result.getCode()) {
                        Alumno_Application.setAlumnos(result.getAlumnos());
                        if (Alumnos_Activity.getAdapter() != null)
                            Alumnos_Activity.getAdapter().updateList();
                    }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(context, "Error: "+ statusCode + responseString, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(context, "Error: "+ statusCode, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void sendMail(final Context context, String to, String subject, String message) {
        final ProgressDialog progreso = new ProgressDialog(context);
        RequestParams params = new RequestParams();
        params.put("from", FROM);
        params.put("password", PASSWORD);
        params.put("to", to);
        params.put("subject", subject);
        params.put("message", message);
        RestClient.post(URL_MAIL, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Enviando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Enviado correctamente";
                        ((Activity)context).finish();
                    } else
                        message = "Error al enviar el correo:\n" + result.getMessage();
                else
                    message = "Null data";
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }
}
